package eu.tamere.rxuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    Subscription mWebServiceSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserManager manager = new UserManager();

        // FIXME: Don't forget to cancel subscription !!!

        mWebServiceSubscription = manager.getUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        users -> Timber.d("Got %d Users", users.size()),
                        throwable -> Timber.e(throwable, "Got error :'("));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mWebServiceSubscription != null) {
            mWebServiceSubscription.unsubscribe();
        }
    }
}
