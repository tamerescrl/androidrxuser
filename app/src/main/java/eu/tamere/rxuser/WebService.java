package eu.tamere.rxuser;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface WebService {

    /**
     * Return Example:
     [
     {
     "id": 1,
     "full_name": "Simon Hill",
     "followers": 7484
     },
     {
     "id": 2,
     "full_name": "Peter Graham",
     "followers": 7019
     },
     {
     "id": 3,
     "full_name": "Angelina Johnston",
     "followers": 2700
     },
     {
     "id": 4,
     "full_name": "Josh Hunt",
     "followers": 3322
     }]
    **/
    @GET("users.json")
    Observable<List<UserHolder>> listUsers();
}
