package eu.tamere.rxuser;

import com.google.gson.annotations.SerializedName;


public class UserHolder {
    @SerializedName("id")
    public int serverId;

    @SerializedName("full_name")
    public String userName;

    @SerializedName("followers")
    public String followerCount;
}
