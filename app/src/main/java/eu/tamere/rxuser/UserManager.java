package eu.tamere.rxuser;


import android.support.annotation.NonNull;

import com.google.common.base.Splitter;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * I provide access to Users
 */
public class UserManager {

    private List<User> cachedUser;

    @NonNull
    private Observable<List<User>> cachedUsers() {
        return Observable.just(cachedUser).filter(result -> result != null);
    }

    @NonNull
    private Observable<List<User>> networkUsers() {
        return App.get().getWebservice().listUsers().map(userHolders -> {
            // We convert UserHolder to plain User for consumption in app
            // FIXME: Quite ugly, there must be a better way!
            ArrayList<User> users = new ArrayList<User>(userHolders.size());
            for(UserHolder holder : userHolders) {
                List<String> name = Splitter.on(" ").splitToList(holder.userName);
                users.add(new User(name.get(0), name.get(1)));
            }

            return users;
        });

        // FIXME: use doOnNext() to put data into cache
    }

    @NonNull
    public Observable<List<User>> getUsers() {
        return Observable.concat(cachedUsers(), networkUsers()).first();
    }
}
