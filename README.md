# RxUser

This small project shows how to use RxJava to retrieve data from a simple webservice. The result of the call is cached. We use it as a simple easy-to-grasp example project.

The app calls http://www.android10.org/myapi/users.json to retrieve a list of users. 

## Inspiration & References

Main reference:

- http://johnpetitto.com/rxjava-android-devs
- https://github.com/jpetitto/rxjava-android-example

Other interesting articles:

- https://medium.com/@diolor/improving-ux-with-rxjava-4440a13b157f#.z27m9y50r
- http://www.philosophicalhacker.com/2015/03/24/how-to-keep-your-rxjava-subscribers-from-leaking/
- http://www.grokkingandroid.com/rxjavas-side-effect-methods/

## Libraries

We use a pretty vanilla Android stack. Except for those libraries:

- Retrolambda
- RxJava / RxAndroid
- Retrofit2
- Timber
- GSon
- Guava
